package pl.parser.nbp.util;

import org.apache.log4j.Logger;
import pl.parser.nbp.currency.CurrencyPosition;

import java.util.List;

/**
 * Created by vreal on 04/10/15.
 */
public class MathUtil {

    final static Logger logger = Logger.getLogger(MathUtil.class);

    public static double countAverageOfBuyingRate(List<CurrencyPosition> list){
        double sum = 0.;
        for(CurrencyPosition pos: list){
            sum += pos.getBuyingRate();
        }
        double result = sum / list.size();
        if(!Double.isFinite(result)){
            logger.warn("countAverageOfBuyingRate returning " + result);
        }
        return result;
    }

    public static double countStandardDeviationOfSellingRate(List<CurrencyPosition> list){
        double sum = 0., sumOfSquares = 0.;
        for(CurrencyPosition position: list){
            sum += position.getSellingRate();
            sumOfSquares += position.getSellingRate() * position.getSellingRate();
        }
        double average = sum / list.size();
        double standardDeviation = Math.sqrt(sumOfSquares / list.size() - average * average);
        if(!Double.isFinite(standardDeviation)){
            logger.warn("countStandardDeviationOfSellingRate returning " + standardDeviation);
        }
        return standardDeviation;
    }
}

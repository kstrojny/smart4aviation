package pl.parser.nbp.connector;

import org.apache.commons.io.IOUtils;
import org.apache.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;
import pl.parser.nbp.MainClass;
import pl.parser.nbp.currency.*;
import pl.parser.nbp.currency.Currency;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by vreal on 04/10/15.
 */
public class NbpConnector {

    public static final String FILE_NAME_FORMAT = MainClass.getProperty("nbp.fileName.format");
    final static Logger logger = Logger.getLogger(NbpConnector.class);

    private DocumentBuilderFactory factory;
    private DateFormat publicationDateFormat;
    private DateFormat fileNameDateFormat;
    private HashMap<String, CurrencyPosition> cache;

    public NbpConnector(){
        factory = DocumentBuilderFactory.newInstance();
        factory.setValidating(false);
        try {
            //tip from: http://stackoverflow.com/questions/22631474/java-io-cannot-find-existing-file-from-url
            factory.setFeature("http://apache.org/xml/features/nonvalidating/load-external-dtd", false);
        } catch (ParserConfigurationException e) {
            logger.error("Fail to set loading external dtd to false", e);
        }
        factory.setIgnoringElementContentWhitespace(true);
        publicationDateFormat = new SimpleDateFormat("yyyy-MM-dd");
        fileNameDateFormat = new SimpleDateFormat("yyMMdd");
        cache = new HashMap<>();
    }

    public List<CurrencyPosition> getCurrencyPositionList(Currency currency, Date startDate, Date endDate, int year){
        return getCurrencyPositionList(currency, startDate, endDate,
                MainClass.getProperty("nbp.url.year").replace("<year>", year + ""));
    }

    public List<CurrencyPosition> getCurrencyPositionList(Currency currency, Date startDate, Date endDate){
        return getCurrencyPositionList(currency, startDate, endDate, MainClass.getProperty("nbp.url"));
    }

    private List<CurrencyPosition> getCurrencyPositionList(Currency currency, Date startDate, Date endDate, String address) {
        try {
            URL dirUrl = new URL(address);
            return getCurrencyPositionList(currency, startDate, endDate, dirUrl);
        } catch(MalformedURLException e){
            logger.error("Problem with nbp url", e);
        }
        return new LinkedList<>();
    }

    private List<CurrencyPosition> getCurrencyPositionList(Currency currency, Date startDate, Date endDate, URL dirUrl) {
        LinkedList<CurrencyPosition> result = new LinkedList<>();
        try {
            InputStream input = dirUrl.openStream();
            String fileNames = IOUtils.toString(input, "UTF-8");
            for(String fileName : fileNames.split("\\r\\n")){
                try {
                    if (isValidFileName(fileName, startDate, endDate)) {
                        CurrencyPosition position;
                        if(cache.containsKey(fileName)){
                            position = cache.get(fileName);
                        } else {
                            position = getCurrencyPosition(currency, fileName);
                            cache.put(fileName, position);
                        }
                        result.add(position);
                    }
                } catch(ParseException e){
                    logger.error("Parse exception", e);
                } catch(CurrencyPositionNotFoundException e){
                    logger.warn("Currency position wasn't find for " + fileName);
                }
            }
        } catch (IOException e) {
            logger.error("Problem with downloading content", e);
        }
        return result;
    }

    private boolean isValidFileName(String fileName, Date startDate, Date endDate) throws ParseException {
        if(fileName.matches(FILE_NAME_FORMAT)) {
            int startOfDate = Integer.parseInt(MainClass.getProperty("nbp.startDateInFileName"));
            Date dateInFileName = fileNameDateFormat.parse(fileName.substring(startOfDate));
            return !dateInFileName.after(endDate) && !startDate.after(dateInFileName);
        }
        return false;
    }

    private CurrencyPosition getCurrencyPosition(Currency currency, String fileName) throws CurrencyPositionNotFoundException {
        try {
            URL xmlUrl = new URL(MainClass.getProperty("nbp.url.xml").replace("<xml>", fileName));
            //no need to close this stream
            InputStream stream = xmlUrl.openConnection().getInputStream();

            DocumentBuilder builder = factory.newDocumentBuilder();
            Document doc = builder.parse(stream);
            doc.getDocumentElement().normalize();

            String publicationString = doc.getElementsByTagName(MainClass.getProperty("publication.date")).item(0).getTextContent();
            Date publicationDate = publicationDateFormat.parse(publicationString);

            NodeList nList = doc.getElementsByTagName(MainClass.getProperty("position"));
            for (int i = 0; i < nList.getLength(); i++) {
                Node node = nList.item(i);
                if (node.getNodeType() == Node.ELEMENT_NODE) {
                    Element element = (Element) node;
                    if (currency.name().equals(element.getElementsByTagName(MainClass.getProperty("currency.code")).item(0).getTextContent())) {
                        String bRate = element.getElementsByTagName(MainClass.getProperty("buying.rate")).item(0).getTextContent();
                        double buyingRate = Double.parseDouble(bRate.replace(',', '.'));
                        String sRate = element.getElementsByTagName(MainClass.getProperty("selling.rate")).item(0).getTextContent();
                        double sellingRate = Double.parseDouble(sRate.replace(',', '.'));
                        return new CurrencyPosition(publicationDate, buyingRate, sellingRate, currency);
                    }
                }
            }
        } catch (MalformedURLException e) {
            logger.error("", e);
        } catch (ParserConfigurationException e) {
            logger.error("", e);
        } catch (SAXException e) {
            logger.error("XML Parsing error", e);
        } catch (ParseException e) {
            logger.error("", e);
        } catch (IOException e) {
            logger.error("", e);
        }
        throw new CurrencyPositionNotFoundException();
    }

}

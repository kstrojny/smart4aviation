package pl.parser.nbp;


import org.apache.log4j.Logger;
import pl.parser.nbp.currency.Currency;
import pl.parser.nbp.currency.CurrencyPositionNotFoundException;
import pl.parser.nbp.currency.CurrencyUtil;

import java.io.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Properties;

public class MainClass {

    private static Properties properties;
    final static Logger logger = Logger.getLogger(MainClass.class);
    public static final DateFormat df = new SimpleDateFormat("yyyy-MM-dd");

    static{
        df.setLenient(false);

        InputStream input = null;
        try {
            properties = new Properties();
            input = new FileInputStream("config.properties");
            properties.load(input);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (input != null) {
                try {
                    input.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
    }
    private static final String HELP_FLAG = "-help";
    private static final int FIRST_YEAR = 2002;

    public static String getProperty(String key) {
        return properties.getProperty(key);
    }

    private static void logError(String msg){
        logger.error(msg);
    }

    private static void printUsageAndExit(){
        System.out.println("Usage: java pl.parser.nbp.MainClass currency startDate endDate");
        System.out.println("Accepted currencies are: USD, EUR, CHF, GBP");
        System.out.println("Data format is: 2015-02-20");
        System.exit(0);
    }

    private static Date parseInputDate(String stringDate) throws ParseException{
        return df.parse(stringDate);
    }

    private static void checkDates(Date startDate, Date endDate){
        if(startDate.after(endDate)){
            logError("Start date is after end date");
            printUsageAndExit();
        }
        Date today = new Date();
        if(!endDate.before(today)){
            logError("End date is after today, you can't ask for future currency");
            printUsageAndExit();
        }
        try {
            Date year2002 = df.parse(FIRST_YEAR + "-01-01");
            if(startDate.before(year2002)){
                logError("Start date is before 2002, nbp does not provide necessary information");
                printUsageAndExit();
            }
        } catch(ParseException e){
            throw new AssertionError(); //should never happen
        }
    }

    public static void main(String[] args) {
        if(args.length == 1 && HELP_FLAG.equalsIgnoreCase(args[0])){
            printUsageAndExit();
        } else if(args.length != 3){
            logError("Wrong number of arguments");
            printUsageAndExit();
        } else {
            try {
                Currency currency = Currency.valueOfIgnoreCase(args[0]);
                Date startDate = parseInputDate(args[1]);
                Date endDate = parseInputDate(args[2]);
                checkDates(startDate, endDate);

                CurrencyUtil currencyUtil = new CurrencyUtil();
                System.out.printf("%.4f\n", currencyUtil.getAverageOfBuyingRate(currency, startDate, endDate));
                System.out.printf("%.4f\n", currencyUtil.getStandardDeviationOfSellingRate(currency, startDate, endDate));

            } catch(IllegalArgumentException e){
                logError("Wrong currency name");
                printUsageAndExit();
            } catch(ParseException e){
                logError("Wrong data format");
                printUsageAndExit();
            } catch(CurrencyPositionNotFoundException e){
                logError("No exchange rate data for given date range");
            }
        }
    }
}

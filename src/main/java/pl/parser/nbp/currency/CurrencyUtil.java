package pl.parser.nbp.currency;

import org.apache.log4j.Logger;
import pl.parser.nbp.util.MathUtil;
import pl.parser.nbp.connector.NbpConnector;

import java.util.*;

/**
 * Created by vreal on 04/10/15.
 */
public class CurrencyUtil {

    final static Logger logger = Logger.getLogger(CurrencyUtil.class);
    private NbpConnector connector;

    public CurrencyUtil(){
        connector = new NbpConnector();
    }

    public double getAverageOfBuyingRate(pl.parser.nbp.currency.Currency currency, Date startDate, Date endDate){
        List<CurrencyPosition> currencyPositions = getCurrencyPositionList(currency, startDate, endDate);
        if(currencyPositions.size() == 0){
            throw new CurrencyPositionNotFoundException();
        }
        return MathUtil.countAverageOfBuyingRate(currencyPositions);
    }

    public double getStandardDeviationOfSellingRate(pl.parser.nbp.currency.Currency currency, Date startDate, Date endDate){
        List<CurrencyPosition> currencyPositions = getCurrencyPositionList(currency, startDate, endDate);
        if(currencyPositions.size() == 0){
            throw new CurrencyPositionNotFoundException();
        }
        return MathUtil.countStandardDeviationOfSellingRate(currencyPositions);
    }

    private List<CurrencyPosition> getCurrencyPositionList(pl.parser.nbp.currency.Currency currency, Date startDate, Date endDate){
        int endYear = getYear(endDate);
        int currentYear = getYear(new Date());

        List<CurrencyPosition> buyingRates = new LinkedList<>();
        for(int year = getYear(startDate); year < currentYear && year <= endYear; year++){
            buyingRates.addAll(connector.getCurrencyPositionList(currency, startDate, endDate, year));
            logger.debug("Finished parsing year = " + year);
        }
        if(endYear == currentYear){
            buyingRates.addAll(connector.getCurrencyPositionList(currency, startDate, endDate));
        }
        return buyingRates;
    }

    private int getYear(Date date){
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        return cal.get(Calendar.YEAR);
    }
    
}

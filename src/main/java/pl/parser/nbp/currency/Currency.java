package pl.parser.nbp.currency;

/**
 * Created by vreal on 04/10/15.
 */
public enum Currency {

    USD, EUR, CHF, GBP;

    public static Currency valueOfIgnoreCase(String name){
        return valueOf(name.toUpperCase());
    }


}

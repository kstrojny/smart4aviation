package pl.parser.nbp.currency;

import java.util.Date;

/**
 * Created by vreal on 05/10/15.
 */
public class CurrencyPosition {

    private Date date;
    private double buyingRate;
    private double sellingRate;
    private Currency currency;

    public CurrencyPosition(Date date, double buyingRate, double sellingRate, Currency currency) {
        this.date = date;
        this.buyingRate = buyingRate;
        this.sellingRate = sellingRate;
        this.currency = currency;
    }

    public Date getDate() {
        return date;
    }

    public double getBuyingRate() {
        return buyingRate;
    }

    public double getSellingRate() {
        return sellingRate;
    }

    public Currency getCurrency() {
        return currency;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o){
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        CurrencyPosition that = (CurrencyPosition) o;

        if (Double.compare(that.buyingRate, buyingRate) != 0) {
            return false;
        }
        if (Double.compare(that.sellingRate, sellingRate) != 0) {
            return false;
        }
        if (!date.equals(that.date)) {
            return false;
        }
        return currency == that.currency;
    }

    @Override
    public int hashCode() {
        int result;
        long temp;
        result = date.hashCode();
        temp = Double.doubleToLongBits(buyingRate);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(sellingRate);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        result = 31 * result + currency.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "CurrencyPosition{" +
                "date=" + date +
                ", buyingRate=" + buyingRate +
                ", sellingRate=" + sellingRate +
                ", currency=" + currency +
                '}';
    }
}

package pl.parser.nbp.currency;

import junit.framework.TestCase;
import org.junit.Test;
import pl.parser.nbp.MainClass;

import java.util.Date;

/**
 * Created by vreal on 05/10/15.
 */
public class CurrencyUtilTest extends TestCase {

    CurrencyUtil util = new CurrencyUtil();

    @Test
    public void testGetAverageOfBuyingRate() throws Exception {
        Date startDate = MainClass.df.parse("2015-01-29");
        Date endDate = MainClass.df.parse("2015-01-29");
        assertEquals(util.getAverageOfBuyingRate(Currency.EUR, startDate, endDate), 4.1956);

        startDate = MainClass.df.parse("2014-06-20");
        endDate = MainClass.df.parse("2014-06-20");
        assertEquals(util.getAverageOfBuyingRate(Currency.EUR, startDate, endDate), 4.0985);

        startDate = MainClass.df.parse("2013-01-28");
        endDate = MainClass.df.parse("2013-01-31");
        assertEquals(util.getAverageOfBuyingRate(Currency.EUR, startDate, endDate), 4.150525);
    }

    @Test//(expected=CurrencyPositionNotFoundException.class)
    public void testCurrencyPositionNotFoundException() throws Exception{
        Date startDate = MainClass.df.parse("2015-01-31");
        Date endDate = MainClass.df.parse("2015-01-31");
        boolean wasException = false;
        try {
            util.getAverageOfBuyingRate(Currency.EUR, startDate, endDate);
        } catch(CurrencyPositionNotFoundException e){
            assertTrue(true);
            wasException = true;
        }
        if(!wasException) {
            assertFalse(true);
        }
    }

    @Test
    public void testGetStandardDeviationOfSellingRate() throws Exception {
        Date startDate = MainClass.df.parse("2015-01-29");
        Date endDate = MainClass.df.parse("2015-01-29");
        assertEquals(util.getStandardDeviationOfSellingRate(Currency.EUR, startDate, endDate), 0.);

        startDate = MainClass.df.parse("2014-06-20");
        endDate = MainClass.df.parse("2014-06-20");
        assertEquals(util.getStandardDeviationOfSellingRate(Currency.EUR, startDate, endDate), 0.);

        startDate = MainClass.df.parse("2013-01-28");
        endDate = MainClass.df.parse("2013-01-31");
        assertEquals(util.getStandardDeviationOfSellingRate(Currency.EUR, startDate, endDate), 0.012477053939129816);
    }
}